module CharList (CharList) where

newtype CharList
  = CharList { getCharList :: [ Char ] }
    deriving (Eq, Show)
