-- # Finding the Right Triangle
--
-- Let’s wrap things up with a problem that combines tuples and list
-- comprehensions. We’ll use Haskell to find a right triangle that fits all of
-- these conditions:
--
--   - The lengths of the three sides are all integers.
--   - The length of each side is less than or equal to 10.
--   - The triangle’s perimeter (the sum of the side lengths) is equal to 24.
--

-- all possible sides with length < 10
triples_all =
  [ (a, b, c)
  | c <- [1..10]
  , b <- [1..10]
  , a <- [1..10]
  ]

-- side length where hypotenuse is the longest
triples_big_hyp =
  [ (a, b, c)
  | c <- [1..10]
  , b <- [1..c]
  , a <- [1..c]
  ]

-- side length where hypotenuse is the longest and excluding duplicates from
-- remaining two sides.
triples_big_hyp_dedup =
  [ (a, b, c)
  | c <- [1..10]
  , b <- [1..c]
  , a <- [1..b]
  ]

-- sides that obey the Pythagoras law
triples_pythagorean =
  [ (a, b, c)
  | c <- [1..10]
  , b <- [1..c]
  , a <- [1..b]
  , a^2 + b^2 == c^2
  ]

-- sides that obey the Pythagoras law
triples_pythagorean_24 =
  [ (a, b, c)
  | c <- [1..10]
  , b <- [1..c]
  , a <- [1..b]
  , a^2 + b^2 == c^2
  , a + b + c == 24
  ]
