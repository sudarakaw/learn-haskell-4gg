module Main (main) where

import Tree

freeTree :: Tree Char
freeTree =
  Node 'P'
    ( Node 'O'
      ( Node 'L'
        ( Node 'N' Empty Empty )
        ( Node 'T' Empty Empty )
      )
      ( Node 'Y'
        ( Node 'S' Empty Empty )
        ( Node 'A' Empty Empty )
      )
    )
    ( Node 'L'
      ( Node 'W'
        ( Node 'C' Empty Empty )
        ( Node 'R' Empty Empty )
      )
      ( Node 'A'
        ( Node 'A' Empty Empty )
        ( Node 'C' Empty Empty )
      )
    )

-- change2P' :: Tree Char -> Tree Char
-- change2P' (Node x lt (Node y (Node _ m n) rt)) =
--   Node x lt (Node y (Node 'P' m n) rt)

-- change2P :: Trail a -> Tree Char -> Tree Char
-- change2P (L:ds) (Node x lt rt) = Node x (change2P ds lt) rt
-- change2P (R:ds) (Node x lt rt) = Node x lt (change2P ds rt)
-- change2P [] (Node _ lt rt) = Node 'P' lt rt

-- elemAt :: Trail a -> Tree a -> Maybe a
-- elemAt _ Empty = Nothing
-- elemAt (L:ds) (Node _ lt _) = elemAt ds lt
-- elemAt (R:ds) (Node _ _ rt) = elemAt ds rt
-- elemAt [] (Node x _ _) = Just x

main :: IO ()
main = do
  print $ freeTree

  -- putStrLn "--------------------------------------------------------------------------------\n"
  -- print $ change2P' freeTree
  -- print $ change2P [R, L] freeTree
  -- print $ elemAt [R, L, L, R] freeTree
  -- print $ (elemAt [R, L]) . (change2P [R, L]) $ freeTree

  -- putStrLn "--------------------------------------------------------------------------------\n"
  -- print $ goLeft . goRight $ (freeTree, [])
  -- print $ (freeTree, []) -: goRight -: goLeft

  putStrLn "--------------------------------------------------------------------------------\n"
  -- print $ (freeTree, []) -: goRight -: goLeft
  -- print $ (freeTree, []) -: goRight -: goLeft -: modify (\_ -> '#')
  -- print $ (freeTree, []) -: goRight -: goLeft -: modify (\_ -> '#') -: goUp -: goUp
  -- let
  --   focus = (freeTree, []) -: goLeft -: goRight -: modify (\_ -> '#')
  --   focus2 = focus -: goUp -: modify (\_ -> '!')
  --
  -- print focus2
  --
  let
    farLeft = (freeTree, []) -: goLeft -: goLeft -: goLeft -: goLeft
    focus3 = farLeft -: attach (Node 'Z' Empty Empty)

  print $ focus3 -: top
