type Birds = Int

type Pole = (Birds, Birds)

{- v1
landLeft :: Birds -> Pole -> Pole
landLeft n (left, right) = (left + n, right)

landRight :: Birds -> Pole -> Pole
landRight n (left, right) = (left, right + n)

(-:) :: Pole -> (Pole -> Pole) -> Pole
x -: f = f x
-}

{- v2
-}
landLeft :: Birds -> Pole -> Maybe Pole
landLeft n (left, right)
  | abs ((left + n) - right) < 4 = Just (left + n, right)
  | otherwise = Nothing

landRight :: Birds -> Pole -> Maybe Pole
landRight n (left, right)
  | abs (left - (right + n)) < 4 = Just (left, right + n)
  | otherwise = Nothing

{- v2.1
-}
banana :: Pole -> Maybe Pole
banana _ = Nothing

{- v2.2
-}
routine :: Maybe Pole
routine = do
  start <- return (0, 0)
  first <- landLeft 2 start
  second <- landRight 2 first
  landLeft 1 second

main = do
  {- v1
  print $ landLeft 2 (0, 0)
  print $ landRight 1 (1, 2)
  print $ landRight 10 (1, 2)
  print $ landRight (-1) (1, 5)

  print $ landLeft 2 (landRight 1 (landLeft 1 (0, 0)))
  print $ (landLeft 2 . landRight 1 . landLeft 1) (0, 0)
  print $ (0, 0) -: landLeft 2 -: landRight 1 -: landLeft 1
  print $ (0, 0) -: landLeft 1 -: landRight 4 -: landLeft (-1) -: landRight (-2)
  -}

  {- v2
  print $ landRight 1 (0, 0) >>= landLeft 2
  print $ Nothing >>= landLeft 2
  print $ return (0, 0) >>= landRight 2 >>= landLeft 2 >>= landRight 2
  print $ return (0, 0) >>= landLeft 1 >>= landRight 4 >>= landLeft (-1) >>= landRight (-2)
  -}

  {- v2.1
  print $ return (0, 0) >>= landRight 2            >>= landLeft 2 >>= landRight 2
  print $ return (0, 0) >>= landRight 2 >>= banana >>= landLeft 2 >>= landRight 2
  -- Note: `>>= banana` above and `>> Nothing` below have the same effect.
  print $ return (0, 0) >>= landRight 2 >> Nothing >>= landLeft 2 >>= landRight 2
  -}

  {- v2.2
  -}
  print routine
