module Main (main) where

import FS

myDisk :: FSItem
myDisk =
  Folder "root"
    [ File "goat_yelling_like_man.wmv" "baaaaaa"
    , File "pope_time.avi" "god bless"
    , Folder "pics"
      [ File "ape_throwing_up.jpg" "bleargh"
      , File "watermelon_smash.gif" "smash!!"
      , File "skull_man(scary).bmp" "Yikes!"
      ]
    , File "dijon_poupon.doc" "best mustard"
    , Folder "programs"
      [ File "fartwizard.exe" "10gotofart"
      , File "owl_bandit.dmg" "mov eax, h00t"
      , File "not_a_virus.exe" "really not a virus"
      , Folder "source code"
        [ File "best_hs_prog.hs" "main = print (fix error)"
          , File "random.hs" "main = print 4"
        ]
      ]
    ]

main :: IO ()
main = do
  print myDisk
  putStrLn "--------------------------------------------------------------------------------\n"

  let
    focus = (myDisk, []) -: fsTo "pics" -: fsTo "skull_man(scary).bmp"
    focus2 = focus -: fsUp -: fsTo "watermelon_smash.gif"

  print $ fst focus
  print $ fst focus2
  putStrLn "--------------------------------------------------------------------------------\n"

  let
    focus3 = (myDisk, []) -: fsTo "pics" -: fsRename "cspi" -: fsUp
    focus4 = (myDisk, []) -: fsTo "pics" -: fsNewFile (File "heh.jpg" "lol") -: fsUp

  print $ fst focus3
  print $ fst focus4
  putStrLn "--------------------------------------------------------------------------------\n"
