
data Pet
  = Doggo
    { name :: String
    , age :: Int
    }
  | Catto
    { name :: String
    , age :: Int
    }
  deriving (Show)


