GHC			= ghc
GHCFLAGS	= -dynamic
BINARIES	= hello namerock hello2 rev capstream shortlines palindrome \
			  girlfriend girlfriendcaps 2do/append 2do/delete args 2do/2do \
			  randstr guess bscp rpn lhr2lon io-functor monad pierre knight \
			  simplelog writer writer-do writer-gcd writer-countdown stack \
			  monad-func toss rpn-safe knight-monad-compose prob-test \
			  tree-walk zipper4list fs-test

.PHONY: all
all: $(BINARIES) $(APPS)

ifdef FILE
.PHONY: watch
watch:
	watch -cn3 "make $(FILE) && ./$(FILE)"
endif

%: %.hs
	$(GHC) $(GHCFLAGS) $<

.PHONY: clean
clean:
	find . -type f -regex '.+\.\(o\|hi\)$$' -delete
	$(RM) temp*
	$(RM) $(BINARIES)
