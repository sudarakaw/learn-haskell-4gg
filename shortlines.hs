-- -- v1
-- main = do
--   contents <- getContents
--
--   putStrLn $ shortLinesOnly contents

-- v2
main =
  interact shortLinesOnly

shortLinesOnly :: String -> String
shortLinesOnly =
  unlines . filter (\line -> length line < 10) . lines
