module Functor where

import Tree

instance Functor Tree where
  fmap f Empty = Empty
  fmap f (Node x lt rt) = Node (f x) (fmap f lt) (fmap f rt)

