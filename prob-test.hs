module Main (main) where

import           Data.Ratio

import           Prob

probs :: [(Int, Rational)]
probs =
  [ (3, 1%2)
  , (5, 1%4)
  , (9, 1%4)
  ]

probinprob :: Prob (Prob Char)
probinprob =
  Prob [ (Prob [('a', 1%2), ('b', 1%2)], 1%4)
       , (Prob [('c', 1%2), ('d', 1%2)], 3%4)
       ]

data Coin
  = Heads
  | Tails
  deriving (Show, Eq)

coin :: Prob Coin
coin = Prob [(Heads, 1%2), (Tails, 1%2)]

loadedCoin :: Prob Coin
loadedCoin = Prob [(Heads, 1%10), (Tails, 9%10)]

flip3 :: Prob Bool
flip3 = do
  a <- coin
  b <- coin
  c <- loadedCoin

  return $ all (== Tails) [a, b, c]

main :: IO ()
main = do
  print [] :: [(String, Rational)]
  print [] :: [(Int, Rational)]
  print $ Prob probs
  print $ fmap negate (Prob probs)
  print probinprob
  print $ probinprob >>= id
  print $ getProb flip3
