module Main (main) where

type ListZipper a = ([a], [a])

forward :: ListZipper a -> ListZipper a
forward ([], zs) = ([], zs)
forward (x:xs, zs) = (xs, x:zs)

backward :: ListZipper a -> ListZipper a
backward (xs, []) = (xs, [])
backward (xs, z:zs) = (z:xs, zs)

main :: IO ()
main = do
  print $ (forward . forward) ([1], [])
  print $ (forward . forward) ([1, 2, 3, 4], [])
  print $ (backward . backward) ([1], [])
  print $ (backward . backward) ([1, 2, 3, 4], [])
  print $ (forward . backward) ([1], [])
  print $ (forward . backward) ([1, 2, 3, 4], [])
  print $ (backward . forward) ([1], [])
  print $ (backward . forward) ([1, 2, 3, 4], [])
