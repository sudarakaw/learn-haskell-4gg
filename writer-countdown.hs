module Main (main) where

import Control.Monad.Writer

import DiffList

slowCountDown :: Int -> Writer [String] ()
slowCountDown 0 = do
  tell ["0"]
slowCountDown x = do
  slowCountDown (x - 1)

  tell [show x]

finalCountDown :: Int -> Writer (DiffList String) ()
finalCountDown 0 = do
  tell $ toDiffList ["0"]
finalCountDown x = do
  finalCountDown (x - 1)

  tell $ toDiffList [show x]

main :: IO ()
main = do
  let
    result = runWriter $ finalCountDown 20000
    -- result = runWriter $ slowCountDown 20000

  print $ fst result

  mapM_ putStrLn $ (fromDiffList . snd) result
  -- mapM_ putStrLn $ snd result
