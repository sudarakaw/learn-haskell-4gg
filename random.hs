import System.Random

toss :: StdGen -> (Bool, Bool, Bool)
toss gen =
  let
    (coin1, gen1) = random gen
    (coin2, gen2) = random gen1
    (coin3, _) = random gen2
  in
    (coin1, coin2, coin3)
