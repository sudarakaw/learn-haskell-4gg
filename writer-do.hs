module Main (main) where

import Control.Monad.Writer

logNum :: Int -> Writer [String] Int
logNum x =
  writer (x, [ "Got number: " ++ show x ])

multWithLog :: Writer [String] Int
multWithLog = do
  a <- logNum 3
  b <- logNum 5

  tell [ "Gonna multiply these two" ]

  return (a * b)

main :: IO ()
main = do
  print $ logNum 5

  print $ multWithLog
  print $ runWriter multWithLog
