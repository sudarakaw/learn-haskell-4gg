
import Tree
import Traffic

class YesNo a where
  yesno :: a -> Bool

instance YesNo Int where
  yesno 0 = False
  yesno _ = True

instance YesNo [a] where
  yesno [] = False
  yesno _ = True

instance YesNo Bool where
  yesno = id

instance YesNo (Maybe a) where
  yesno (Just _) = True
  yesno Nothing = False

instance YesNo (Tree a) where
  yesno Empty = False
  yesno _ = True

instance YesNo Light where
  yesno Red = False
  yesno _ = True


yesnoIf :: (YesNo y) => y -> a -> a -> a
yesnoIf ynVal yResult nResult =
  if yesno ynVal
    then yResult
    else nResult
