module Main (main) where

import Control.Monad.Writer

-- gcd' :: Int -> Int -> Int
-- gcd' a b
--   | 0 == b = a
--   | otherwise = gcd' b (a `mod` b)

gcd' :: Int -> Int -> Writer [String] Int
gcd' a b
  -- | 0 == b =
  --     writer (a, ["Finish with " ++ show a])

  | 0 == b = do
      tell ["Finish with " ++ show a]

      return a

  | otherwise = do
      tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b) ]

      gcd' b (a `mod` b)

main :: IO ()
main = do
  let
    result = runWriter (gcd' 8 3)

  print $ fst result

  mapM_ putStrLn $ snd result
