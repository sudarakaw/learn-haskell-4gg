module FS (FSItem(..), fsUp, fsTo, fsRename, fsNewFile, (-:)) where

type Name = String
type Data = String

data FSItem
  = File Name Data
  | Folder Name [FSItem]
  deriving (Show)

data FSPrt = FSPrt Name [FSItem] [FSItem]
  deriving (Show)

type FSZipper = (FSItem, [FSPrt])

fsUp :: FSZipper -> FSZipper
fsUp (item, FSPrt name ls rs:ps) = (Folder name (ls ++ [item] ++ rs), ps)

fsTo :: Name -> FSZipper -> FSZipper
fsTo name (Folder fname items, ps) =
  let
    (ls, item:ps') = break (nameIs name) items
  in
    (item, FSPrt fname ls ps':ps)

fsRename :: Name -> FSZipper -> FSZipper
fsRename name (Folder _ items, ps) = (Folder name items, ps)
fsRename name (File _ _data, ps) = (File name _data, ps)

fsNewFile :: FSItem -> FSZipper -> FSZipper
fsNewFile item (Folder name items, ps) = (Folder name (item:items), ps)

nameIs :: Name -> FSItem -> Bool
nameIs name (Folder fname _) = name  == fname
nameIs name (File fname _) = name  == fname

-- compose FS traversal
infixl 1 -:
(-:) :: FSZipper -> (FSZipper -> FSZipper) -> FSZipper
x -: f = f x
