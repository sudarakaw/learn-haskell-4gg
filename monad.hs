
applyMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
applyMaybe Nothing _ = Nothing
applyMaybe (Just x) f = f x

main = do
  -- print $ (\x -> Just (1 + x)) 1

  putStrLn "\napplyMaybe to Just value"
  print $ Just 3 `applyMaybe` \x -> Just (1 + x)
  print $ Just "smile" `applyMaybe` \x -> Just (x ++ " :)")

  putStrLn "\napplyMaybe to Nothing"
  print $ Nothing `applyMaybe` \x -> Just (1 + x)
  print $ Nothing `applyMaybe` \x -> Just (x ++ " :)")

  putStrLn "\napplyMaybe with function returning Just value"
  print $ Just 3 `applyMaybe` \x -> if 2 < x then Just (1 + x) else Nothing

  putStrLn "\napplyMaybe with function returning Nothing"
  print $ Just 1 `applyMaybe` \x -> if 2 < x then Just (1 + x) else Nothing
