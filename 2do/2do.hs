import Control.Exception
import Data.List
import System.Directory
import System.Environment
import System.IO

dispatch :: String -> [String] -> IO ()
dispatch "add" = add
dispatch "view" = view
dispatch "remove" = remove
dispatch cmd = doesntExist cmd

doesntExist :: String -> [String] -> IO ()
doesntExist cmd _ =
  putStrLn $ "The \"" ++ cmd ++ "\" command doesn't exist"

add :: [String] -> IO ()
add [fileName, item] = appendFile fileName (item ++ "\n")
add _ = putStrLn "The \"add\" command takes exactly two arguments"

view :: [String] -> IO ()
view [fileName] = do
  contents <- readFile fileName

  let
    todos = lines contents
    numberedTasks = zipWith (\n line -> show n ++ " - " ++ line) [0..] todos

  putStr $ unlines numberedTasks
view _ = putStrLn "The \"view\" command takes exactly one argument"

remove :: [String] -> IO ()
remove [fileName, numberString] = do
  contents <- readFile fileName

  let
    todos = lines contents
    numberedTasks = zipWith (\n line -> show n ++ " - " ++ line) [0..] todos
    number = read numberString
    newTodoItems = unlines $ delete (todos !! number) todos

  putStrLn "These are your TO-DO items:"
  mapM putStrLn numberedTasks

  bracketOnError
    (openTempFile "." "temp")

    (\(tempName, tempHandle) -> do
      hClose tempHandle

      removeFile tempName
    )

    (\(tempName, tempHandle) -> do
      hPutStr tempHandle newTodoItems
      hClose tempHandle

      removeFile fileName
      renameFile tempName fileName
    )
remove _ = putStrLn "The \"remove\" command takes exactly two arguments"

main = do
  (cmd : args) <- getArgs

  dispatch cmd args
