import System.IO
import System.Directory
import Data.List
import Control.Exception

main = do
  -- Read TO-DO content
  contents <- readFile "todo.txt"

  let
    -- Split TO-DO content into lines (one line per item)
    todos = lines contents
    -- Make each line with index number starting with zero
    numberedTasks = zipWith (\n line -> show n ++ " - " ++ line) [0..] todos

  putStrLn "These are your TO-DO items:"

  -- Print formatted DO-DO item per line on screen
  mapM_ putStrLn numberedTasks

  numberString <- getLine

  let
    -- Cast string/character to number
    number = read numberString
    -- NOTE:
    --  `(!!) list num` returns the element at num position of the list
    --  `delete item list` return a new list without the given item
    newTodoItems = unlines $ delete (todos !! number) todos

  -- Capture exceptions & clear temporary file
  bracketOnError
    -- create & open (for writing) a temporary file names `.temp????`
    (openTempFile "." "temp")

    -- On error
    (\(tempName, tempHandle) -> do
      hClose tempHandle
      removeFile tempName
    )

    -- On success
    (\(tempName, tempHandle) -> do
      -- Save updated TO-DO list to temporary file
      hPutStr tempHandle newTodoItems

      hClose tempHandle

      -- Replace `todo.txt` with the temporary file
      removeFile "todo.txt"
      renameFile tempName "todo.txt"
    )
