
doubleMe x =
  x + x

doubleUs x y =
  doubleMe x + doubleMe y

doubleSmallNumber x =
  if 100 < x
     then x
     else doubleMe x
