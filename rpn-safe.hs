module Main (main) where

import Control.Monad

solve_rpn :: String -> Maybe Double
solve_rpn str = do
  [result] <- foldM foldingFunction [] (words str)

  return result

foldingFunction :: [Double] -> String -> Maybe [Double]
foldingFunction (x:y:ys) "*" = return $ (y * x) : ys
foldingFunction (x:y:ys) "+" = return $ (y + x) : ys
foldingFunction (x:y:ys) "-" = return $ (y - x) : ys
foldingFunction (x:y:ys) "/" = return $ (y / x) : ys
foldingFunction (x:y:ys) "^" = return $ (y ** x) : ys
foldingFunction (x:xs) "ln" = return $ log x : xs
foldingFunction xs "sum" = return [sum xs]
foldingFunction xs num = liftM (: xs) (readMaybe num)

readMaybe :: (Read a) => String -> Maybe a
readMaybe str =
  case reads str of
    [(x, "")] -> Just x
    _ -> Nothing

main :: IO [()]
main = do
  mapM
    (\exp -> do
      putStrLn $ "\"" ++ exp ++ "\" -> " ++ show (solve_rpn exp)
    )
    [ "10 4 3 + 2 * -"
    , "2 3.5 +"
    , "90 34 12 33 55 66 + * - +"
    , "90 34 12 33 55 66 + * - + -"
    , "90 3.8 -"
    , "2.7 ln"
    , "10 10 10 10 sum 4 /"
    , "10 10 ? 10 10 10 sum 4 /"
    , "10 10 10 10 10 sum 4 /"
    , "ala 8i kola 2i"
    , "10 2 ^"
    ]
