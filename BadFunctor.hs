
data CMaybe a
  = CNothing
  | CJust Int a
    deriving (Show)

instance Functor CMaybe where
  fmap f CNothing = CNothing
  fmap f (CJust counter x) = CJust (1 + counter) (f x)

