module List (List) where

-- -- List with normal constructor
-- data List a
--   = Empty
--   | Cons a (List a)
--   deriving (Show)

-- List with infix constructor `:>`
infixr 1 :>  -- makes the constructor `:>` right associative.
data List a
  = Empty
  | a :> (List a)
  deriving (Show)

-- Add two Lists together with infix function/operator `##`
infixr 1 ##
(##) :: List a -> List a -> List a
Empty     ## ys = ys
(x :> xs) ## ys = x :> xs ## ys  -- parenthesis obsolete bcs of
                                 -- right associativity.
                                 -- x :> ( xs ## ys )
