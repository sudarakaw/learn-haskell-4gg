module Main (main, stackManip) where

import Control.Monad.State

type Stack = [Int]

{- v1
pop :: Stack -> (Int, Stack)
pop (x:xs) =
  (x, xs)

push :: Int -> Stack -> ((), Stack)
push a xs =
  ((), a:xs)

stackManip :: Stack -> (Int, Stack)
stackManip stack =
  let
    ((), stack') = push 3 stack
    (a, stack'') = pop stack'
  in
    pop stack''
-}

{- v2
pop :: State Stack Int
pop =
  state $ \(x:xs) -> (x, xs)

push :: Int -> State Stack ()
push a =
  state $ \xs -> ((), a:xs)
-}

stackManip :: State Stack Int
stackManip = do
  push 3
  pop
  pop

stackStuff :: State Stack ()
stackStuff = do
  a <- pop

  if 5 == a
     then push 5
     else do
       push 3
       push 8

moreStack :: State Stack ()
moreStack = do
  a <- stackManip

  if 100 == a
     then stackStuff
     else return ()

stackyStack :: State Stack ()
stackyStack = do
  stackNow <- get

  if [1, 2, 3] == stackNow
     then put [8, 3, 1]
     else put [9, 2, 1]

{- v3
-}
pop :: State Stack Int
pop = do
  (x:xs) <- get
  put xs

  return x

push :: Int -> State Stack ()
push x = do
  xs <- get

  put (x:xs)

main :: IO ()
main = do
  print $ runState stackManip [5, 8, 2, 1]
  print $ runState stackStuff [9, 0, 2, 1, 0]
  print $ runState moreStack [100, 200, 300, 400]
  print $ runState stackyStack [1,2,3]
