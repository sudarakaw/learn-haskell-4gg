
data Section
  = Section { a :: Int, b :: Int, c :: Int }
    deriving (Show)

type RoadSystem
  = [ Section ]

data Label
  = A
  | B
  | C
    deriving (Show)

type Path
  = [ (Label, Int ) ]

optimalPath :: RoadSystem -> Path
optimalPath roadSystem =
  let
    (bestA, bestB) = foldl roadStep ([], []) roadSystem

    timeA = sum $ map snd bestA
    timeB = sum $ map snd bestB
  in
    if timeA <= timeB
       then reverse bestA
       else reverse bestB

roadStep :: (Path, Path) -> Section -> (Path, Path)
roadStep (pathA, pathB) (Section a b c) =
  let
    timeA = sum $ map snd pathA
    timeB = sum $ map snd pathB

    travelA = timeA + a
    crossA = timeB + b + c
    travelB = timeB + b
    crossB = timeA + a + c

    newA = if travelA <= crossA
              then (A, a) : pathA
              else (C, c) : (B, b) : pathB

    newB = if travelB <= crossB
              then (B, b) : pathB
              else (C, c) : (A, a) : pathA
  in
    (newA, newB)

groupsOf :: Int -> [a] -> [[a]]
groupsOf 0 _ = undefined
groupsOf _ [] = []
groupsOf n xs = take n xs : groupsOf n (drop n xs)

lhr2lon :: RoadSystem
lhr2lon =
  [ Section 50 10 30
  , Section 5 90 20
  , Section 40 2 25
  , Section 10 8 0
  ]

main = do
  contents <- getContents

  let
    threes = groupsOf 3 (map read $ lines contents)
    roadSystem = map (\[a, b, c] -> Section a b c) threes
    path = optimalPath roadSystem

    pathString = concat $ map (show . fst) path
    pathTime = sum $ map snd path

  putStrLn $ "The best path to take is: " ++ pathString
  putStrLn $ "Time taken: " ++ show pathTime
