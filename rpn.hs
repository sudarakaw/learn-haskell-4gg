
solve_rpn :: String -> Double
solve_rpn =
  head . foldl f [] . words
    where f (x:y:ys) "*" = (y * x) : ys
          f (x:y:ys) "+" = (y + x) : ys
          f (x:y:ys) "-" = (y - x) : ys
          f (x:y:ys) "/" = (y / x) : ys
          f (x:y:ys) "^" = (y ** x) : ys
          f (x:xs) "ln" = log x : xs
          f xs "sum" = [sum xs]
          f xs num = read num : xs

main = do
  mapM
    (\exp -> do
      putStrLn $ "\"" ++ exp ++ "\" -> " ++ show (solve_rpn exp)
    )
    [ "10 4 3 + 2 * -"
    , "2 3.5 +"
    , "90 34 12 33 55 66 + * - +"
    , "90 34 12 33 55 66 + * - + -"
    , "90 3.8 -"
    , "2.7 ln"
    , "10 10 10 10 sum 4 /"
    , "10 10 10 10 10 sum 4 /"
    , "10 2 ^"
    ]
