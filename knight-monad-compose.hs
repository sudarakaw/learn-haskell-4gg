module Main (main) where

import Control.Monad

type Pos = (Int, Int)

moveKnight :: Pos -> [Pos]
moveKnight (c, r) = do
  (c', r') <- [ (c + 2, r + 1)
              , (c + 2, r - 1)
              , (c - 2, r + 1)
              , (c - 2, r - 1)
              , (c + 1, r + 2)
              , (c + 1, r - 2)
              , (c - 1, r + 2)
              , (c - 1, r - 2)
              ]

  guard (c' `elem` [1..8] && r' `elem` [1..8])

  return (c', r')

-- moveKnight :: Pos -> [Pos]
-- moveKnight (c, r) = filter onBoard
--   [ (c + 2, r + 1)
--   , (c + 2, r - 1)
--   , (c - 2, r + 1)
--   , (c - 2, r - 1)
--   , (c + 1, r + 2)
--   , (c + 1, r - 2)
--   , (c - 1, r + 2)
--   , (c - 1, r - 2)
--   ]
--   where
--     onBoard (c, r) = c `elem` [1..8] && r `elem` [1..8]

-- in3 :: Pos -> [Pos]
-- in3 start = do
--   first <- moveKnight start
--   second <- moveKnight first
--
--   moveKnight second

in3 :: Pos -> [Pos]
in3 start =
  return start
    >>= moveKnight
    >>= moveKnight
    >>= moveKnight

canReachIn3 :: Pos -> Pos -> Bool
canReachIn3 start end =
  end `elem` in3 start

inMoves ::Int -> Pos -> [Pos]
inMoves n start =
  return start
    -- compose `n` of monadic function `moveKnight` using `<=<` operator.
    >>= foldr (<=<)return (replicate n moveKnight)

canReachIn :: Int -> Pos -> Pos -> Bool
canReachIn n start end =
  end `elem` inMoves n start

main :: IO ()
main = do
  print $ canReachIn3 (6, 2) (6, 1)
  print $ canReachIn3 (6, 2) (7, 3)
  print $ canReachIn 3 (6, 2) (6, 1)
  print $ canReachIn 3 (6, 2) (7, 3)
  print $ canReachIn 1 (6, 2) (6, 1)
  print $ canReachIn 1 (6, 2) (8, 3)
