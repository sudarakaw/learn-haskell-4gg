module Main (main) where

import Control.Monad
import Data.Monoid

newtype Writer w a = Writer { runWriter :: (a, w) }

instance (Monoid w) => Functor (Writer w) where
  fmap = liftM

instance (Monoid w) => Applicative (Writer w) where
  pure = return
  (<*>) = ap

instance (Monoid w) => Monad (Writer w) where
  return x = Writer (x, mempty)
  (Writer (x, v)) >>= f =
    let
      (Writer (y, v')) = f x
    in
      Writer (y, v `mappend` v')

main :: IO ()
main = do
  print $ runWriter (return 3 :: Writer String Int)
  print $ runWriter (return 3 :: Writer (Sum Int) Int)
  print $ runWriter (return 3 :: Writer (Product Int) Int)
