module Main (main) where

import Data.Monoid

type Food = String
type Price = Sum Int

addDrink :: Food -> (Food, Price)
addDrink "beans" = ("milk", Sum 25)
addDrink "jerky" = ("whiskey", Sum 99)
addDrink _ = ("beer", Sum 30)

isBigGang :: Int -> (Bool, String)
isBigGang x = (9 < x, "Compared gang size to 9.")

applyLog :: (Monoid m) => (a, m) -> (a -> (b, m)) -> (b, m)
applyLog (x, log) f =
  let
    (y, newLog) = f x
  in
    (y, log `mappend` newLog)

main :: IO ()
main = do
  print $ isBigGang 3
  print $ isBigGang 30

  print $ (3, "Smallish gang.") `applyLog` isBigGang
  print $ (30, "A freaking platoon.") `applyLog` isBigGang

  print $ ("Tobin", "Go outlaw name.") `applyLog` \x -> (length x, "Applied length.")
  print $ ("Bathcat", "Go outlaw name.") `applyLog` \x -> (length x, "Applied length.")

  print $ Sum 3 `mappend` Sum 9

  print $ ("beans", Sum 10) `applyLog` addDrink
  print $ ("jerky", Sum 25) `applyLog` addDrink
  print $ ("dogmeat", Sum 5) `applyLog` addDrink
  print $ ("dogmeat", Sum 5) `applyLog` addDrink `applyLog` addDrink
