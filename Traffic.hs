module Traffic (Light(..)) where

data Light'
  = Red'
  | Yellow'
  | Green'
  deriving (Eq, Show)

data Light
  = Red
  | Yellow
  | Green

instance Eq Light where
  Red == Red = True
  Yellow == Yellow = True
  Green == Green = True
  _ == _ = False

instance Show Light where
  show Red = "Red light"
  show Yellow = "Yellow light"
  show Green = "Green light"
