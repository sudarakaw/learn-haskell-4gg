import Data.Char
import Data.List

-- -- v1
-- main = do
--   line' <- getLine
--   let line = reverse line'
--
--   putStrLn $ "You said \"" ++ line ++ "\" backwards!"
--   putStrLn $ "Yes, you said \"" ++ line ++ "\" backwards!"

-- -- v2 (with `fmap`)
-- main = do
--   line <- fmap reverse getLine
--
--   putStrLn $ "You said \"" ++ line ++ "\" backwards!"
--   putStrLn $ "Yes, you said \"" ++ line ++ "\" backwards!"

-- v3 (`fmap` composed functions)
main = do
  line <- fmap (intersperse '~' . map toUpper . reverse) getLine

  putStrLn line
