module Tree
  ( Tree(..)
  , attach
  , goLeft
  , goRight
  , goUp
  , modify
  , top
  , (->>)
  , (-:)) where

data Tree a
  = Empty
  | Node a (Tree a) (Tree a)
  deriving (Show)

data Dir a
  = L a (Tree a)
  | R a (Tree a)
  deriving (Show)

type Zipper a = (Tree a, [Dir a])

-- traverse left branch
goLeft :: Zipper a -> Zipper a
goLeft (Node x lt rt, bs) = (lt, L x rt : bs)

-- traverse right branch
goRight :: Zipper a -> Zipper a
goRight (Node x lt rt, bs) = (rt, R x lt : bs)

-- traverse up using the direction list (trail)
goUp :: Zipper a -> Zipper a
goUp (lt, (L x rt) : bs) = (Node x lt rt, bs)
goUp (rt, (R x lt) : bs) = (Node x lt rt, bs)

top :: Zipper a -> Zipper a
top (t, []) = (t, [])
top bs      = top . goUp $ bs

-- compose tree traversal
infixl 1 -:
(-:) :: Zipper a -> (Zipper a -> Zipper a) -> Zipper a
x -: f = f x

-- modify Tree node value
modify :: (a -> a) -> Zipper a -> Zipper a
modify f (Empty, bs)        = (Empty, bs)
modify f (Node x lt rt, bs) = (Node (f x) lt rt, bs)

-- insert a new Tree
attach :: Tree a -> Zipper a -> Zipper a
attach t (_, bs) = (t, bs)

-- create a new tree with single node
singleton :: a -> Tree a
singleton x = Node x Empty Empty

-- insert value into existing tree
infixr 1 ->>
(->>) :: (Ord a) => a -> Tree a -> Tree a
x ->> Empty = singleton x
x ->> (Node a lt rt)
  | x == a = Node x lt rt
  | x < a  = Node a (x ->> lt) rt
  | x > a  = Node a lt (x ->> rt)

-- check if value is in the tree
(-?) :: (Ord a) => a -> Tree a -> Bool
x -? Empty = False
x -? (Node a lt rt)
  | x == a = True
  | x < a  = x -? lt
  | x > a  = x -? rt
