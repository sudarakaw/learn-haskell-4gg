import           Control.Exception
import qualified Data.ByteString.Lazy as B
import           System.Directory
import           System.Environment
import           System.IO

copy :: String -> String -> IO ()
copy src dst = do
  contents <- B.readFile src

  bracketOnError
    (openTempFile "." "temp")

    (\(tempName, tempHandle) -> do
      hClose tempHandle

      removeFile tempName
    )

    (\(tempName, tempHandle) -> do
      B.hPutStr tempHandle contents

      hClose tempHandle

      renameFile tempName dst
    )

main = do
  (fileName1 : fileName2 : _) <- getArgs

  copy fileName1 fileName2
