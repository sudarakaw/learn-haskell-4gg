
import Data.Char

main = do
  putStrLn "What's your given name?"
  fName <- getLine

  putStrLn "What's your family name?"
  lName <- getLine

  let
    bigFName = map toUpper fName
    bigLName = map toUpper lName

  putStrLn $ "Hey " ++ bigFName ++ " " ++ bigLName
    ++ ", how are you?"
