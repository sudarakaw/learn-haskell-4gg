-- -- v1
-- import Data.Char
-- import Control.Monad
--
-- main = forever $ do
--   l <- getLine
--
--   putStrLn $ map toUpper l

-- v2
import Data.Char

main = do
  content <- getContents

  putStrLn $ map toUpper content
