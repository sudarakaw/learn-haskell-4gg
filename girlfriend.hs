import System.IO

-- -- v1
-- main = do
--   handle <- openFile "girlfriend.txt" ReadMode
--   contents <- hGetContents handle
--
--   putStr contents
--
--   hClose handle

-- -- v2
-- main = do
--   withFile "girlfriend.txt" ReadMode (\handle -> do
--     contents <- hGetContents handle
--
--     putStr contents)

-- v3
main = do
  contents <- readFile "girlfriend.txt"

  putStr contents
