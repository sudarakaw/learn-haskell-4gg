import System.Random
import Control.Monad (when)

-- -- v1
-- askForNumber ::  StdGen -> IO ()
-- askForNumber gen = do
--   let
--     (randNum, newGen) = randomR (1,10) gen :: (Int, StdGen)
--
--   putStrLn "Which number in the range from 1 to 10 am I thinking of?"
--   numberS <- getLine
--
--   when (not $ null numberS) $ do
--     let
--       number = read numberS
--
--     if randNum == number
--        then putStrLn "You are correct!"
--        else putStrLn $ "Sorry, it was " ++ show randNum
--
--     askForNumber newGen
--
-- main = do
--   gen <- getStdGen
--
--   askForNumber gen

-- v2
main = do
  -- v2.1
  -- gen <- getStdGen

  -- v2.2
  gen <- newStdGen

  let
    (randNum, _) = randomR (1, 10) gen :: (Int, StdGen)

  putStrLn "Which number in the range from 1 to 10 am I thinking of?"
  numberS <- getLine

  when (not $ null numberS) $ do
    let
      number = read numberS

    if randNum == number
       then putStrLn "You are correct!"
       else putStrLn $ "Sorry, it was " ++ show randNum

    -- v2.1
    -- newStdGen

    main
