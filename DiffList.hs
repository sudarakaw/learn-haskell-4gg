module DiffList (DiffList, toDiffList, fromDiffList) where

newtype DiffList a =
  DiffList { getDiffList :: [a] -> [a] }

instance Semigroup (DiffList a) where
  (DiffList f) <> (DiffList g) =
    DiffList (\xs -> f (g xs))

instance Monoid (DiffList a) where
  mempty = DiffList (\xs -> [] ++ xs)

toDiffList :: [a] -> DiffList a
toDiffList xs =
  DiffList (xs ++)

fromDiffList :: DiffList a -> [a]
fromDiffList (DiffList f) =
  f []

-- main :: IO ()
-- main = do
--   print $ fromDiffList $ toDiffList [1, 2, 3, 4] `mappend` toDiffList [1, 2, 3]
--   print $ fromDiffList $ toDiffList [1, 2, 3, 4] <> toDiffList [1, 2, 3]
