-- import qualified Data.Foldable as F

import Tree

instance Foldable Tree where
  foldMap f Empty = mempty
  foldMap f (Node x lt rt) = foldMap f lt `mappend`
                             f x `mappend`
                             foldMap f rt


