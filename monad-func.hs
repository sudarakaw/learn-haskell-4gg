module Main (main) where

addStuff :: Int -> Int
addStuff = do
  a <- (* 2)
  b <- (+ 10)

  return (a + b)

main :: IO ()
main = do
  print $ addStuff 5
